
<!DOCTYPE html>
<html lang="es-ES" prefix="og: http://ogp.me/ns#">
<meta charset="UTF-8">
<title>Sign up</title>
<link rel="stylesheet" type="text/css" href="css/styles.css" media="all" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<head></head>

<body class="login-page fade-in">

    <div class="login-wrapper">

        <h1 class="login-title">Sign up</h1>

        <form class="login-form" name="register" action="php/register-management.php" method="POST">
            <p class="login-text">Username</p>
            <input class="login-input" type="text" name="register_username" value="" />
            <p class="login-text">Password</p>
            <input class="login-input" type="password" name="register_password" value="" />
            <button class="confirm-login" action="submit" name="register">Sign up</button>
        </form>

    </div>

</body>