<!DOCTYPE html>
<html lang="es-ES" prefix="og: http://ogp.me/ns#">
<meta charset="UTF-8">
<title>Shopping cart</title>
<link rel="stylesheet" type="text/css" href="../css/styles.css" media="all" />
<link rel="icon" type="image/x-icon" href="./favicon.png" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<script src="js/loadCustomerBeats.js"></script>
</head>

<body class="fade-in">

    <h1 class="shopping-title">Shopping history</h1>

    <div class="table-wrapper">

        <table id="myTable">
            <tr class="header">
                <th style="width:25%;">Cover</th>
                <th style="width:15%;">Name</th>
                <th style="width:15%;">Key</th>
                <th style="width:15%;">Bpm</th>
                <th style="width:15%;">License</th>
                <th style="width:15%;"></th>

            </tr>
        </table>

    </div>

</body>