
<!DOCTYPE html>
<html lang="es-ES" prefix="og: http://ogp.me/ns#">
<meta charset="UTF-8">
<title>Mandi beatstore</title>
<link rel="stylesheet" type="text/css" href="css/styles.css" media="all" />
<link rel="icon" type="image/x-icon" href="./favicon.png" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

</head>

<body class="details-body fade-in">
	
	<?php include './php/getbeatdetails.php'; ?>

</body>

<script src="js/details-audioplayer.js"></script>